/** Basic arithmetic operations */
const mylib = {
    /**Multiline arrow function */
    add: (a,b) => {
        const sum = a + b;
        return sum;
    },
    subtrack: (a, b) => {
        return a - b;
    },
    /**Singleline arrow function */
    divide: (dividend, divisor) => dividend / divisor,
    /**Tavallinen funktio */
    multiply: function(a,b){
        return a * b;
    }
};

module.exports = mylib;