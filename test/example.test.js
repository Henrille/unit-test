const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() =>{
        console.log("Initialising tests..");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3 for some reason?");
    });
    it("Can subtrack 1 from 2", () => {
        // Tests
        expect(mylib.subtrack(2,1)).equal(1, "2 - 1 is not 1 for some reason?");
    });
    it("Can divide 15 with 3", () => {
        // Tests
        expect(mylib.divide(15,3)).equal(5, "15 / 3 is not 5 for some reason?");
    });
    it("Can multiply 2 with 6", () => {
        // Tests
        expect(mylib.multiply(2,6)).equal(12, "2 * 6 is not 12 for some reason?");
    });
    after(() => {
        //Cleanup
        // For example: shutdown the Express server.
        console.log("Testing complete.")
    });
});